import nxt
import pygame
import bluetooth as bt
import serial
import atexit
from flask import Flask, render_template, request, send_from_directory
import multiprocessing
import time

app = Flask(__name__)

joystick0_robot = ''
joystick1_robot = ''

class Display:
    def __init__(self):
        self.status = "idle"
        self.display = serial.Serial('/dev/ttyACM0', 9600)
        self.msg = ""
    def update_display(self):
        self.display.write(b'\xFE\x58\xFE\xD0\x00\x00\xFF')
        self.display.write(bytes(self.msg, 'utf8'))
        self.display.write(b'\xFE\x47')
        self.display.write(bytes('17STATUS: ' + self.status, 'utf8'))
    def write_msg(self,str):
        self.msg = str
        self.update_display()
    def change_status(self,str):
        self.status = str
        self.update_display()
    def manual(self):
        return self.display

lcd = Display()

def scary_exit():
    print('exit')
    manual_lcd = lcd.manual()
    manual_lcd.write(b'\xFE\x58\xFE\xD0\xFF\x00\x00')
    manual_lcd.write(bytes("Failure!", 'utf8'))
    manual_lcd.write(b'\xFE\x47')
    manual_lcd.write(bytes("17Software error!", 'utf8'))
atexit.register(scary_exit)


def robot_control(joystick, robot_name):
    #print ('this does something eventually')
    #print(joystick)
    pygame.joystick.Joystick(joystick).init()

    prev_joystick_vals = [0,0]

    brick = nxt.locator.find_one_brick(name = robot_name, debug=True)
    print('brick connect')

    l_motor = nxt.Motor(brick, nxt.PORT_B)
    r_motor = nxt.Motor(brick, nxt.PORT_C)

    while True:
        pygame.event.pump()
        y_axis = round(pygame.joystick.Joystick(joystick).get_axis(1))
        x_axis = round(pygame.joystick.Joystick(joystick).get_axis(0))

        if prev_joystick_vals[0] != x_axis or prev_joystick_vals[1] != y_axis:
            prev_joystick_vals = [x_axis, y_axis]
            print(prev_joystick_vals)

            if y_axis == -1 and x_axis == 0:
                print('forward')
                l_motor.run()
                r_motor.run()
            elif y_axis == 1 and x_axis == 0:
                print('backward')
                l_motor.run(-100)
                r_motor.run(-100)
            elif x_axis == -1:
                print('left')
                l_motor.run(80)
                r_motor.run(-80)
            elif x_axis == 1:
                l_motor.run(-80)
                r_motor.run(80)
                print('right')
            else:
                l_motor.run(0)
                r_motor.run(0)

@app.route('/')
def dashboard():
    return render_template('dashboard.html')
@app.route('/bluetooth', methods=['POST', 'GET'])
def bluetooth():
    if(request.method == 'POST'):
        nearby_devices = bt.discover_devices(lookup_names=True, flush_cache=True)
        print(nearby_devices)
        return "{devices:" + str(nearby_devices) + "}"
    else:
        return render_template('bluetooth.html')
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@app.route('/match', methods=['POST', 'GET'])
def match():
    if(request.method == 'POST'):
        print(request.form['time']) 
        process = multiprocessing.Process(target=robot_control, args=[0, joystick0_robot])
        process1 = multiprocessing.Process(target=robot_control, args=[1, joystick1_robot])

        process.start()
        process1.start()

        seconds = 0
       
        while seconds < int(request.form['time']):
                print('wait')
                seconds = seconds + 1
                time.sleep(1)
        print('time up')
        process = None
        process1 = None
        
        return 'success'
    else:
        return render_template('match.html')

@app.route('/config', methods=['POST'])
def config():
    joystick0_robot = request.form['robot0']
    joystick1_robot = request.form['robot1']
    print(joystick0_robot)
    print(joystick1_robot)
    return 'success'
lcd.change_status('IDLE')
lcd.write_msg('Started!')
pygame.display.init()
pygame.joystick.init()
app.run(host='0.0.0.0')

