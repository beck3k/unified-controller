var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var server = require('http').Server(app)
var io = require('socket.io')(server);
const child_process = require('child_process').spawn;
var unifiedSubprocess = null;
var matchInterval = null;

function beginMatchProcess(time) {
    var i = 0;
    matchInterval = setInterval(() => {
        i++;
        io.emit('time', 60 - i);
        if(i >= time) {
            unifiedSubprocess.kill('SIGINT');
            clearInterval(matchInterval);
            console.log('Status > Match ended');
        }
    }, 1000);
}

function spawnSubprocess(robot, time){
    unifiedSubprocess = child_process('python3', ['./control.py', robot]);
    unifiedSubprocess.stdout.on('data', (data) => {
        console.log('Python Subprocess > ' + data.toString());
        if(data.toString().indexOf("CONTROL>READY") >= 0){
            console.log('Status > ' + 'Controller Ready');
            beginMatchProcess(time);
            unifiedSubprocess.stdin.write("READY\n")
            console.log('Status > Match started');
        }

    });
}

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/config.html');
});

io.on('connection', (socket) => {
    socket.on('get_status', (msg) => {
        console.log(msg);
    })
});

app.use(express.urlencoded());
app.use(express.json());
app.post('/match/start', (req, res) => {
    console.log('Status > Match Starting...')
    console.log('Handler > Subsystem spawning...');
    //console.log(req.body);
    spawnSubprocess(req.body.robot, req.body.time);
    res.send('K');
});

app.post('/match/kill', (req, res) => {
    clearInterval(matchInterval);
    unifiedSubprocess.kill('SIGINT');
    res.send('K');
});

server.listen(8000, () => {
	console.log('Express > Webserver started');
});
