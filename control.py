import sys
import nxt
import pygame
import multiprocessing
import time

pygame.display.init()
pygame.joystick.init()
pygame.joystick.Joystick(0).init()

brick = nxt.locator.find_one_brick(name = sys.argv[1])

l_motor = nxt.Motor(brick, nxt.PORT_B)
r_motor = nxt.Motor(brick, nxt.PORT_C)

joystick = [0,0]

print('CONTROL>READY')
sys.stdout.flush()

while True:
    pygame.event.pump()

    y_axis = round(pygame.joystick.Joystick(0).get_axis(1))
    x_axis = round(pygame.joystick.Joystick(0).get_axis(0))

    if joystick[0] != x_axis or joystick[1] != y_axis:
        joystick = [x_axis, y_axis]

        if y_axis == -1 and x_axis == 0:
            l_motor.run()
            r_motor.run()
        elif y_axis == 1 and x_axis == 0:
            l_motor.run(-100)
            r_motor.run(-100)
        elif x_axis == -1:
            l_motor.run(80)
            r_motor.run(-80)
        elif x_axis == 1:
            l_motor.run(-80)
            r_motor.run(80)
        else:
            l_motor.run(0)
            r_motor.run(1)
